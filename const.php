<?php
define("QUESTIONS", array(
    '0' => 'Do  you love me ?', '1' => '2 + 1 = ?',
    '2' => '3 + 1 = ?', '3' => '4 + 1 = ?',
    '4' => '5 + 1 = ?', '5' => '1 + 6 = ?',
    '6' => '2 / 1 = ?', '7' => '3 + 1 = ?',
    '8' => '2 x 3 = ?', '9' => '5 + 10 = ?',
));
define("ANSWERS", array(
    '0' => array('answers' => array('yes', 'no'), 'result' => 'yes'),
    '1' => array('answers' => array(2, 3, 4, 5), 'result' => 3),
    '2' => array('answers' => array(2, 3, 4, 5), 'result' => 4),
    '3' => array('answers' => array(2, 3, 4, 5), 'result' => 5),
    '4' => array('answers' => array(1, 3, 6, 9), 'result' => 6),
    '5' => array('answers' => array(7, 2, 5, 0), 'result' => 7),
    '6' => array('answers' => array(2, 3, 4, 5), 'result' => 2),
    '7' => array('answers' => array(2, 3, 4, 5), 'result' => 4),
    '8' => array('answers' => array(2, 3, 6, 5), 'result' => 6),
    '9' => array('answers' => array(2, 0, 15, 5), 'result' => 15),
));
define("LIMIT", 5);